{include file="admin/_head.tpl" title="Créer un document — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=1}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="facture"}

<style>
{literal}
#Line1 > .fact_rm_line {
	display: none;
}
{{/literal}}
</style>

{form_errors}

<form method="post" action="{$self_url}">
	<ul class="actions">
		<li><input type="radio" name="type" value="facture" {form_field data=$radio name=type checked=facture default=facture} id="f_type_facture"/><label for="f_type_facture">Facture</label></li>
		<li><input type="radio" name="type" value="devis" {form_field data=$radio name=type checked=devis} id="f_type_devis" /><label for="f_type_devis">Devis</label></li>
		<li><input type="radio" name="type" value="cerfa" {form_field data=$radio name=type checked=cerfa} id="f_type_cerfa"/><label for="f_type_cerfa">Reçu fiscal</label></li>
		<li><a href="{plugin_url file="cotis_ajouter.php"}"/>Reçu de cotisaition</a></li>
	</ul>

	<fieldset>
		<legend>Créer une facture</legend>
		<dl>

			<dt><label for="f_numero_facture">Numéro facture</label> <b title="(Champ obligatoire et unique)">obligatoire et unique</b></dt>
			<dd><input type="numero" name="numero_facture" maxlength="12" id="f_numero_facture" value="{form_field name=numero_facture}"/></dd>
			<p> Chaque facture doit comporter un numéro unique délivré chronologiquement et de façon continue.<br>Il faut que le système adopté par l'association garantisse que deux factures émises la même année ne peuvent pas porter le même numéro. </p>
			<br>
			
			<dt><label for="f_date_emission">Date d'émission</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
			<dd><input type="date" name="date_emission" id="f_date_emission" size="10" required="required" value="{form_field name=date_emission}"/></dd>
		  
			<dt><label for="f_date_echeance">Date d'échéance</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
			<dd><input type="date" name="date_echeance" id="f_date_echeance" size="10" required="required" value="{form_field name=date_echeance}"/></dd>

			<dt><label for="f_reglee">Réglée</label></dt>
			<dd><input type="checkbox" name="reglee" id="f_reglee" {form_field name=reglee checked=on default=off}></dd>

			<dt><label for="f_archivee">Archivée</label></dt>
			<dd><input type="checkbox" name="archivee" id="f_archivee" disabled></dd>

		</dl>
	</fieldset>

	<fieldset>
		<legend>Client</legend>

		<dl>
			<dt><label>Facture adressée à un·e :</label></dt>
			<dd>
				<input type="radio" name="base_receveur" value="membre" id="f_base_membre" {form_field name=base_receveur checked=membre}/><label for="f_base_membre"> Membre</label>
				<input type="radio" name="base_receveur" value="client" id="f_base_client" {form_field name=base_receveur checked=client default=client}/><label for="f_base_client"> Client·e</label>
			</dd>

			<dt><label>Client</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
			<dd>
				<select class="type_membre" name="membre" id="f_membre" required="required">
				{foreach from=$membres item="membre"}
					<option value="{$membre.id}"{if $membre.id == $membre_id} selected="selected"{/if}>{$membre->$identite}</option>
				{/foreach}
				</select>
				<select class="type_client" name="client" id="f_client" required="required">
				{foreach from=$clients item="client"}
					<option value="{$client.id}"{if $client.id == $client_id} selected="selected"{/if}>{$client.nom}</option>
				{/foreach}
				</select>
			</dd>
		</dl>
	</fieldset>

	<fieldset>
		<legend>Contenu</legend>

		<dl>
            <dt><label for="f_moyen_paiement">Moyen de paiement</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd>
                <select name="moyen_paiement" id="f_moyen_paiement" required="required">
                {foreach from=$moyens_paiement item="moyen"}
                    <option value="{$moyen.code}"{if $moyen.code == $moyen_paiement} selected="selected"{/if}>{$moyen.nom}</option>
                {/foreach}
                </select>
            </dd>

			<dt><label for="f_contenu">Contenu du document</label><dt>
			<dd>
				<table class="list" style="max-width: 800px;">
					<colgroup>
						<col width="65%">
						<col width="33%">
						<col width="2%">
					</colgroup>
					<thead>
						<tr>
							<td>Désignation</td>
							<td>Prix</td>
							<td></td>
						</tr>
					</thead>
					<tbody id="Lines">
						{if count($designations) > 0}
						<tr id="Line1" class="hidden">
							<td><textarea name="designation[]" style="width:98%;"></textarea></td>
							<td><input type="number" step="0.01" value="0" style="width: 60%" onchange="updateSum();" name="prix[]"><span style="position: relative;right: 50px;">€</span></td>
							<td class="fact_rm_line"><button type="button" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);updateSum();">Supprimer</button></td>
						</tr>

						{foreach from=$designations item=designation key=key}
							
						<tr>
							<td><textarea name="designation[]" style="width:98%;">{$designation}</textarea></td>
							<td><input type="number" step="0.01" value="{$prix[$key]}" style="width: 60%" onchange="updateSum();" name="prix[]"><span style="position: relative;right: 50px;">€</span></td>
							<td class="fact_rm_line"><button type="button" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);updateSum();">Supprimer</button></td>
						</tr>

						{/foreach}


						{else}						
						<tr id="Line1">
							<td><textarea name="designation[]" style="width:98%;"></textarea></td>
							<td><input type="number" step="0.01" value="0" style="width: 60%" onchange="updateSum();" name="prix[]"><span style="position: relative;right: 50px;">€</span></td>
							<td class="fact_rm_line"><button type="button" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);updateSum();">Supprimer</button></td>
						</tr>
						{/if}
					</tbody>
					<tfoot>
						<tr>
							<td style="text-align: right;">Total :</td>
							<td><span id="total">0.00</span> €</td>
							<td></td>
						</tr>
					</tfoot>
				</table>
				<button type="button" id="ajouter_ligne">Ajouter une ligne</button>
			</dd>
		</dl>
	</fieldset>

	{include file="%s/templates/_js.tpl"|args:$plugin_root}

	<p class="submit">
		{csrf_field key="ajout_facture"}
		<input type="submit" name="add" value="Enregistrer &rarr;" />
	</p>
</form>

{include file="admin/_foot.tpl"}

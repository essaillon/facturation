{include file="admin/_head.tpl" title="Configuration — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="config"}

{if $ok && !$form->hasErrors()}
    <p class="confirm">
        La configuration a bien été enregistrée.
    </p>
{/if}

{form_errors}

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Informations de l'association</legend>
        <dl>
            <dt><label for="f_siret">RNA de l'assocation</label></dt>
            <dd><input type="text" id="f_rna" name="rna_asso" value="{form_field data=$plugin.config name=rna_asso}"></dd>
            
            <dt><label for="f_siret">SIRET de l'assocation</label></dt>
            <dd><input type="text" id="f_siret" name="siret_asso" value="{form_field data=$plugin.config name=siret_asso}"></dd>
        </dl>
        <br>
        <fieldset>
            <legend>Adresse</legend>
            <dl>
            
            <dt><label for="f_numero_rue">Numéro de rue</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" id="f_numero_rue" maxlength=5 name="numero_rue" value="{form_field data=$plugin.config name=numero_rue_asso}" /></dd>
            
            <dt><label for="f_rue">Rue</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" id="f_rue" name="rue" value="{form_field data=$plugin.config name=rue_asso}" /></dd>
            
            <dt><label for="f_codepostal">Code postal</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" id="f_codepostal" name="codepostal" value="{form_field data=$plugin.config name=cp_asso}" /></dd>
            
            <dt><label for="f_ville">Ville</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" id="f_ville" name="ville" value="{form_field data=$plugin.config name=ville_asso}" /></dd>

            </dl>
        </fieldset>
            <fieldset>
                <legend>Objet</legend>
                <dl>
                    <dt><label>L'objet (but) de l'association doit tenir sur 3 lignes, chaque ligne pouvant accueillir un maximum de 100 caractères.</label><b title="(Champ obligatoire)">obligatoire pour reçus fiscaux</b></dt>
                    <dd><input type="text" maxlength=95 name="objet_0" value="{form_field data=$plugin.config name=objet_0}" /></dd>
                    <dd><input type="text" maxlength=95 name="objet_1" value="{form_field data=$plugin.config name=objet_1}" /></dd>
                    <dd><input type="text" maxlength=95 name="objet_2" value="{form_field data=$plugin.config name=objet_2}" /></dd>
                </dl>
            </fieldset>

            <fieldset>
                <legend>Droit à la réduction d'impôt</legend>
                <dl>
                    <dt><label>Articles concernés par l'association :</label> <b title="(Champ obligatoire)">obligatoire pour reçus fiscaux</b></dt>
                    <dt><input type="checkbox" name="droit_art200" {form_field name="droit_art200" checked=1 data=$plugin.config } /><label>Article 200</label></dt>
                    <dt><input type="checkbox" name="droit_art238bis" {form_field name="droit_art238bis" checked=1 data=$plugin.config } /><label>Article 238 bis</label></dt>
                    <dt><input type="checkbox" name="droit_art885-0VbisA" {form_field name="droit_art885-0VbisA" checked=1 data=$plugin.config } /><label>Article 885-0V bis A</label></dt>
                </dl>
            </fieldset>

    </fieldset>

    <fieldset>
        <legend>Factures</legend>
        <dl>
            <dt><label for="f_footer">Pied de documents/informations légales</label></dt>
            <dd><textarea name="footer" id="f_footer" cols="50" rows="5">{form_field data=$plugin.config name=footer}</textarea></dd>

        </dl>
    </fieldset>

    <fieldset>
        <legend>Configuration du plugin</legend>
        <dl>
            <dt><input type="checkbox" name="validate_cp" id="f_validate_cp" {form_field data=$plugin.config name=validate_cp checked=1}> <label for="f_validate_cp">Vérifier le code postal lors de saisie/modification de client (seulement FR)</label></dt>
            <dt><input type="checkbox" name="unique_name" id="f_unique_name" {form_field data=$plugin.config name=unique_client_name checked=1}> <label for="f_unique_name">Noms des clients uniques</label></dt>
        
        </dl>
        <i>Pour personnaliser l'apparence de la facture, il faut pour l'instant se retrousser les manches et éditer soi-même le fichier www/admin/pdf.php du plugin ! </i>
    </fieldset>

    <p class="submit">
        {csrf_field key="facturation_config"}
        <input type="submit" name="save" value="Enregistrer &rarr;" />
    </p>
</form>


<form method="post" enctype="multipart/form-data" action="{$self_url|escape}" id="f_upload">
    <fieldset>
        <legend>Signature du responsable</legend>

        L'image de la signature doit être au format PNG, d'une taille raisonable et doit être dotée d'un fond transparent.

        <br>
        {* <img src="{$image}" /> *}

        <input type="hidden" name="MAX_FILE_SIZE" value="{$max_size|escape}" id="f_maxsize" />
        <dl>
            <dd class="help">Taille maximale : {$max_size|format_bytes}</dd>
            <dd class="fileUpload"><input type="file" name="fichier" id="f_fichier" data-hash-check /></dd>
        </dl>
        <p class="submit">
            {csrf_field key="signature_config"}
            <input type="submit" name="upload" id="f_submit" value="Envoyer le fichier" />
        </p>
    </fieldset>
</form>


{include file="admin/_foot.tpl"}
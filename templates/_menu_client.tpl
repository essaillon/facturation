<ul class="actions">
    <li{if $current == 'clients'} class="current"{/if}><a href="{plugin_url file="clients.php"}">Liste clients</a></li>
    <li{if $current == 'client'} class="current"{/if}><a href="{plugin_url file="client.php"}?id={$client.id}">{$client.nom}</a></li>
    {if $session->canAccess('compta', Membres::DROIT_ECRITURE)}
        <li{if $current == 'client_modifier'} class="current"{/if}>
            <a href="{plugin_url file="client_modifier.php"}?id={$client.id}">Modifier</a></li>{/if}
    {if $session->canAccess('compta', Membres::DROIT_ADMIN)}
        <li{if $current == 'client_supprimer'} class="current"{/if}>
            <a href="{plugin_url file="client_supprimer.php"}?id={$client.id}">Supprimer</a></li>{/if}
</ul>

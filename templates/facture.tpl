{include file="admin/_head.tpl" title="Document — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{form_errors}

{if $session->canAccess('compta', Membres::DROIT_ECRITURE)}
    {if $type == 3}
        <a href="{plugin_url file="cotis_modifier.php"}?id={$facture.id}">
    {else}
        <a href="{plugin_url file="facture_modifier.php"}?id={$facture.id}">
    {/if}
<button type="button" class="btn btn-primary">Modifier ce document</button></a>
{/if}

<a href="{plugin_url file="pdf.php"}?d&id={$facture.id}">
<button type="button" class="btn btn-primary">Télécharger ce document</button></a>

<div>
    <embed src="pdf.php?id={$id}" type="application/pdf" width="100%" height="800px;" style="max-width: 900px;">
</div>
{include file="admin/_foot.tpl"}

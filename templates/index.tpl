{include file="admin/_head.tpl" title="Documents — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{form_errors}

<table class="list">
<thead>
    <td>Type</td>
    <td>Numéro</td>
    <td>Receveur</td>
    <td>Son adresse</td>
    <td>Sa ville</td>
    <td>Emission</td>
    <td>Echéance</td>
    <td>Réglée</td>
    <td>Archivée</td>
    <td>Moyen paiement</td>
    <td>Contenu</td>
    <td>Total</td>
</thead>
<tbody>
{foreach from=$factures item=facture}
<tr>
    <td><?php switch($facture->type_facture) {
            case 0:
                echo 'Devis';
                break;
            case 1:
                echo 'Facture';
                break;
            case 2:
                echo 'Reçu fiscal';
                break;
            case 3:
                echo 'Reçu cotisation';
                break;
        }
        ?></td>
    <td><a href="{plugin_url file="facture.php"}?id={$facture.id}">{$facture.numero}</a></td>
    {if $facture.receveur_membre}
    <td><a href="{$admin_url}membres/fiche.php?id={$facture.receveur.id}">{$facture.receveur->$identite}</a></td>
    {else}
    <td><a href="{plugin_url file="client.php"}?id={$facture.receveur.id}">{$facture.receveur.nom}</a></td>
    {/if}
    <td>{$facture.receveur.adresse}</td>
    <td>{$facture.receveur.ville}</td>
    <td>{$facture.date_emission|date_fr:'d/m/Y'}</td>
    <td>{$facture.date_echeance|date_fr:'d/m/Y'}</td>
    <td><?= $facture->reglee?'Réglée':'Non' ?></td>
    <td><?= $facture->archivee?'Archivée':'Non' ?></td>
    <td>{$facture.moyen_paiement}</td>
    <td>
        {foreach from=$facture.contenu item=contenu}
        <p>{$contenu.designation} : {$contenu.prix|escape|html_money}&nbsp;{$config.monnaie}</p>
        {/foreach}
    </td>
    <td>{$facture.total|escape|html_money}&nbsp;{$config.monnaie}</td>
</tr>
{/foreach}
</tbody>

</table>

{include file="admin/_foot.tpl"}

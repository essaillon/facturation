{include file="admin/_head.tpl" title="Client — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=0}
{include file="%s/templates/_menu_client.tpl"|args:$plugin_root current="client"}

<dl class="describe" style="display: inline-block;">
    <dt>Numéro de client</dt>
    <dd><p>{$client.id}</p></dd>

    
    <dt>Nom</dt>
    <dd><p>{$client.nom|escape|rtrim|nl2br}</p>
    
    <dt>Adresse</dt>
    <dd><p>{$client.adresse|escape|rtrim|nl2br}</p></dd>
    
    <dt>Ville</dt>
    <dd><p>{$client.ville|escape|rtrim|nl2br}</p></dd>
    
    <dt>Code postal</dt>
    <dd><p>{$client.code_postal|escape|rtrim|nl2br}</p></dd>

    <dt>Adresse électronique</dt>
    <dd>
    {if empty($client.email)}
        <em>(Non renseigné)</em>
    {else}
        <a href="mailto:{$client.email|escape:'url'}">{$client.email}</a>
    {/if}
    </dd>

    <dt>Numéro de téléphone</dt>
    <dd>
    {if empty($client.telephone)}
        <em>(Non renseigné)</em>
    {else}
        <a href="tel:{$client.telephone}">{$client.telephone|format_tel}</a>
    {/if}
    </dd>

    <dt>Date d'ajout</dt>
    <dd>{$client.date_creation|date_fr:'d/m/Y'}</dd>

</dl>

{if $docs}
<div style="display: block;">
<h4>Documents associés</h4>
<table class="list">
<thead>
    <td>Type</td>
    <td>Numéro</td>
    <td>Emission</td>
    <td>Echéance</td>
    <td>Réglée</td>
    <td>Archivée</td>
    <td>Moyen paiement</td>
    <td>Contenu</td>
    <td>Total</td>
</thead>
<tbody>
{foreach from=$docs item=facture}
<tr>
    <td>{if $facture.type_facture == 1}Facture{else}Devis{/if}</td>
    <td><a href="{plugin_url file="facture.php"}?id={$facture.id}">{$facture.numero}</a></td>
    <td>{$facture.date_emission|date_fr:'d/m/Y'}</td>
    <td>{$facture.date_echeance|date_fr:'d/m/Y'}</td>
    <td>{$facture.reglee}</td>
    <td>{$facture.archivee}</td>
    <td>{$facture.moyen_paiement}</td>
    <td>
        {foreach from=$facture.contenu item=contenu}
        <p>{$contenu.designation} : {$contenu.prix|escape|html_money}&nbsp;{$config.monnaie}</p>
        {/foreach}
    </td>
    <td>{$facture.total|escape|html_money}&nbsp;{$config.monnaie}</td>
</tr>
{/foreach}
</tbody>

</table>
</div>
{else}
<h4>Cet utilisateur n'a pas de document associé.</h4>
{/if}

{include file="admin/_foot.tpl"}
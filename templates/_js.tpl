<script type="text/javascript">
    {literal}

	function updateSum(){
		var total = 0;
		e = document.querySelectorAll('input[name="prix[]"]');
		e.forEach( function sum(item, index){
			total = total + Number(item.value);
		});
		document.getElementById('total').innerHTML = total.toFixed(2);
	}

    (function () {

		function plus(){
			var newdiv = document.createElement('tr');
			newdiv.innerHTML = document.getElementById('Line1').innerHTML;
			document.getElementById('Lines').appendChild(newdiv);
		}
		plus();

		$('#ajouter_ligne').onclick = plus;

        function changeTypeSaisie(type)
        {
            g.toggle(['.type_client', '.type_membre'], false);
            g.toggle('.type_' + type, true);
        }

        changeTypeSaisie(document.forms[0].base_receveur.value);

        var inputs = $('input[name="base_receveur"]');

        for (var i = 0; i < inputs.length; i++)
        {
            inputs[i].onchange = function (e) {
                changeTypeSaisie(this.value);
            };
        }

    } ());
    {/literal}
</script>
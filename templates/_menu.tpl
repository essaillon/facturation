<ul class="actions">
    <li{if $current == 'index'} class="current"{/if}><a href="{plugin_url file=""}">Liste documents</a></li>
    {if $session->canAccess('compta', Membres::DROIT_ECRITURE)}
    <li{if $current == 'facture'} class="current"{/if}><a href="{plugin_url file="facture_ajouter.php"}">Nouveau document</a></li>
    {/if}
    <li{if $current == 'clients'} class="current"{/if}><a href="{plugin_url file="clients.php"}">Liste clients</a></li>
    {if $session->canAccess('compta', Membres::DROIT_ADMIN)}
    <li{if $current == 'config'} class="current"{/if}><a href="{plugin_url file="config.php"}">Configuration</a></li>
    {/if}
    <li{if $current == 'aide'} class="current"{/if}><a href="{plugin_url file="aide.php"}">Aide</a></li>
</ul>

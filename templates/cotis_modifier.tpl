{include file="admin/_head.tpl" title="Modifier un reçu — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=1}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

<style>
{literal}
#Line1 > .fact_rm_line {
	display: none;
}
{{/literal}}
</style>

{form_errors}

<form method="post" action="{$self_url}">
	<ul class="actions">
		<li><a href="{plugin_url file="facture_ajouter.php?t=1"}">Facture</a></li>
		<li><a href="{plugin_url file="facture_ajouter.php?t=0"}">Devis</a></li>
		<li><a href="{plugin_url file="facture_ajouter.php?t=2"}">Reçu fiscal</a></li>
		<li class="current"><a href="{$self_url}">Reçu de cotisaition</a></li>
	</ul>

	<fieldset>
		<legend>Créer une reçu de cotisation</legend>
		<dl>
			<dt><label for="f_numero_facture">Numéro de reçu</label> <b title="(Champ obligatoire et unique)">obligatoire et unique</b></dt>
			<dd><input type="numero" name="numero_facture" maxlength="12" id="f_numero_facture" value="{form_field data=$values name=numero_facture}"/></dd>
			<p> Chaque facture doit comporter un numéro unique délivré chronologiquement et de façon continue.<br>Il faut que le système adopté par l'association garantisse que deux factures émises la même année ne peuvent pas porter le même numéro. </p>
			<br>
			
			<dt><label for="f_date_emission">Date d'édition</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
			<dd><input type="date" name="date_emission" id="f_date_emission" size="10" required="required" value="{form_field data=$values name=date_emission}"/></dd>
		  
			<dt><label for="f_archivee">Archivé</label></dt>
			<dd><input type="checkbox" name="archivee" id="f_archivee" disabled></dd>

		</dl>
	</fieldset>

	<fieldset>
		<legend>Membre</legend>
		<dl>
			<dt><label>Reçu adressée à  :</label></dt>
			<dd>
				<select class="type_membre" name="membre" id="f_membre" required="required">
				{foreach from=$membres item="membre"}
					<option value="{$membre.id}"{if $membre.id == $membre_id} selected="selected"{/if}>{$membre->$identite}</option>
				{/foreach}
				</select>
			</dd>
		</dl>
	</fieldset>

	<p class="submit">
		{csrf_field key="add_cotis_1"}
		<input type="submit" name="select" value="Sélectionner &rarr;" />
	</p>


{* {if $step} *}

	<fieldset>
		<legend>Cotisation</legend>
		<dl>
			<dt>Sélectionnez la cotisation concernée :</dt>

			<table class='list'>
				<thead>
					<td></td>
					<td>Id</td>
					<td>Intitulé</td>
					<td>Montant</td>
					<td>Date (souscrit)</td>
					<td>Expiration (calculée)</td>
				</thead>

				{foreach from=$liste item=cotis key=i}
					<tr>
						<td>
							<input type="radio" name="cotisation" value="cotis_{$i}" id="f_cotis_{$i}"{if $radio == "cotis_{$i}"} checked{/if} >
						</td>
						{foreach from=$cotis item=element key=key}
							<td>
									<label for="f_cotis_{$i}">
									{if ($key == 'date' || $key == 'expiration') && $element > 0}
										{$element|date_fr:'d/m/Y'}
									{else}
										{$element}
									{/if}

										<input type="hidden" name="{$key}_{$i}" value="{$element}">
									</label>
							</td>
						{/foreach}
					</tr>
				{/foreach}
			</table>

		</dl>
	</fieldset>

	<p class="submit">
		{csrf_field key="add_cotis_2"}
		<input type="submit" name="add" value="Enregistrer &rarr;" />
	</p>
</form>
{* {/if} *}

{include file="%s/templates/_js.tpl"|args:$plugin_root}
{include file="admin/_foot.tpl"}

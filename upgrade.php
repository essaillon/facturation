<?php

namespace Garradin;

$db = DB::getInstance();
$infos = $plugin->getInfos();
// Pl il y avait cette ligne ?
// $db->import(dirname(__FILE__) . "/data/schema.sql");

// 0.2.0 - Stock le contenu en json plutôt qu'en serialized
if (version_compare($infos->version, '0.2.0', '<'))
{
    $r = (array) DB::getInstance()->get('SELECT * FROM plugin_facturation_factures');
    
    foreach ($r as $e) {
        $e->contenu =json_encode(unserialize((string) $e->contenu));
        $db->update('plugin_facturation_factures', $e, $db->where('id', (int)$e->id));
    }
}

// 0.3.0 - Migration Facturation\Config vers la table plugins
if (version_compare($infos->version, '0.3.0', '<'))
{
    $conf = $db->getAssoc('SELECT cle, valeur FROM plugin_facturation_config ORDER BY cle;');
    foreach($conf as $k=>$v)
    {
        if(!$plugin->setConfig($k, $v))
        {
            throw new UserException('Erreur dans la conversion de la configuration pour la clé : '.$k);
        }
    }
    $db->exec('DROP TABLE `plugin_facturation_config`;');
}
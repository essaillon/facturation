<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess('compta', Membres::DROIT_ADMIN);

qv(['id' => 'required|numeric']);

$id = (int) qg('id');

$c = $client->get($id);

if (!$client)
{
	throw new UserException("Ce client n'existe pas.");
}

if (f('delete'))
{
	$form->check('delete_client_'.$c->id);

	if (!$form->hasErrors())
	{
		try {
			$client->delete($c->id);
			Utils::redirect(PLUGIN_URL . 'clients.php');
	}
	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}
    }
}


$tpl->assign('deletable', $client->isDeletable($id));
$tpl->assign('client', $client->get($id));
$tpl->display(PLUGIN_ROOT . '/templates/client_supprimer.tpl');

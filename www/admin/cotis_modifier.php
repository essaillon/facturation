<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess('compta', Membres::DROIT_ECRITURE);

use Garradin\DB;

$db = DB::getInstance();


qv(['id' => 'required|numeric']);
$id = (int) qg('id');

if (!$f = $facture->get($id))
{
	throw new UserException("Ce document n'existe pas.");
}

$fields = $facture->recu_fields;

$membre_id = f('membre') ?: $f->receveur_id;

$values['numero_facture'] = $f->numero;
$values['date_emission'] = date('Y-m-d', $f->date_emission);

$radio = '';
$liste = [];


if (f('select'))
{
	$form->check('add_cotis_1',[
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date',
		'membre' => 'required|numeric',
	]);

}
elseif (f('add'))
{
	$form->check('add_cotis_2',[
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date',
		'membre' => 'required|numeric',
		'cotisation' => 'required',
		]);

	$radio = f('cotisation');
	
	if (!$form->hasErrors())
	{
		try
		{
			$num = (int) str_replace('cotis_', '', $radio);
			foreach($fields as $field)
			{
				$cotis[$field] = f($field.'_'.$num);
			}

			$cotis['date'] = date('Y-m-d', $cotis['date']);
			$cotis['expiration'] = date('Y-m-d', $cotis['expiration']);

			$r = $db->get('SELECT moyen_paiement, montant FROM membres_operations AS mo INNER JOIN compta_journal AS cj ON cj.id = mo.id_operation
						WHERE mo.id_cotisation = ?;', (int)$cotis['id']);
			$r = $r[0];

			$data = [
				'type_facture' => 3,
				'numero' => f('numero_facture'),
				'receveur_membre' => 1,
				'receveur_id' => f('membre'),
				'date_emission' => f('date_emission'),
				'moyen_paiement' => $r->moyen_paiement,
				'total' => $r->montant,
				'contenu' => ['id' => $cotis['id'],
							'intitule' => $cotis['intitule'],
							'souscription' => $cotis['date'],
							'expiration' => $cotis['expiration'] ]
			];

			if($facture->edit($id, $data))
			{
				Utils::redirect(PLUGIN_URL . 'facture.php?id='.(int)$id);
			}
			throw new UserException('Erreur d\'édition du reçu');
		}
		catch (UserException $e)
		{
			$form->addError($e->getMessage());
		}
	}

}


try
{
	$r = $facture->getCotis((int)$membre_id);
	// Garde seulement les champs requis
	// Rattrape le bouton radio à pré-sélectionner
	// Passe les expiration nulles (cotis ponctuelle) à 0 pour avoir moins d'embrouilles
	foreach ($r as $i=>$cotis)
	{
		foreach($cotis as $k=>$v)
		{
			if (in_array($k, $fields))
			{
				$liste[$i][$k] = $v;
			}
		}

		if($liste[$i]['id'] == $f->contenu['id'])
		{
			$radio = 'cotis_'.$i;
		}

		if($liste[$i]['expiration'] < 0)
		{
			$liste[$i]['expiration'] = 0;
		}
	}
}
catch (UserException $e)
{
	$form->addError($e->getMessage());
}


$tpl->assign('liste', $liste);
$tpl->assign('radio', $radio);
$tpl->assign('values', $values);

// $tpl->assign('step', $step);
$tpl->assign('identite', $identite);
$tpl->assign('membre_id', $membre_id);
$tpl->assign('membres', (array)DB::getInstance()->get('SELECT id, '.$identite.' FROM membres WHERE id_categorie != -2 NOT IN (SELECT id FROM membres_categories WHERE cacher = 1);'));

$tpl->display(PLUGIN_ROOT . '/templates/cotis_modifier.tpl');

<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess('compta', Membres::DROIT_ACCES);

if(f('add'))
{
	$form->check('add_client', [
		'nom' => 'required|string',
		'adresse' => 'required|string',
		'code_postal' => 'required|string',
		'ville' => 'required|string',
		'telephone' => 'string',
		'email' => 'email'
	]);

	if (!$form->hasErrors())
	{
		try
		{
			$id = $client->add([
				'nom'			=>  f('nom'),
				'adresse'		=>  f('adresse'),
				'code_postal'	=>  f('code_postal'),
				'ville'			=>  f('ville'),
				'telephone'		=>  f('telephone'),
				'email'			=>  f('email')
			]);

			$id ? Utils::redirect(PLUGIN_URL . 'client.php?id='.(int)$id):'';
		}
		catch (UserException $e)
		{
			$form->addError($e->getMessage());
		}
	}
}


$tpl->assign('clients', $client->listAll());
$tpl->assign('champs',
	[
		'id' => 'id',
		'nom' => 'Nom',
		'adresse' => 'Adresse',
		'code_postal' => 'Code postal',
		'ville' => 'Ville',
		'telephone' => 'Numéro de téléphone',
		'email' => 'Adresse mail'
	]
);

$tpl->display(PLUGIN_ROOT . '/templates/clients.tpl');

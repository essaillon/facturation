<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess('compta', Membres::DROIT_ACCES);

$tpl->display(PLUGIN_ROOT . '/templates/aide.tpl');
<?php

namespace Garradin;

use Garradin\Plugin\Facturation\Facture;
use Garradin\Plugin\Facturation\Client;
use Garradin\Plugin\Facturation\GenDon;

$client = new Client;
$facture = new Facture;

$identite = (string) Config::getInstance()->get('champ_identite');
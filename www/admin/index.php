<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess('compta', Membres::DROIT_ACCES);

$membres = new Membres;
$cats = new Compta\Categories;

$tpl->assign('moyens_paiement', $cats->listMoyensPaiement());

foreach($factures = $facture->listAll() as $k=>$f)
{
	$factures[$k]->receveur = $f->receveur_membre? $membres->get($f->receveur_id) : $client->get($f->receveur_id);
	$factures[$k]->moyen_paiement = $cats->getMoyenPaiement($f->moyen_paiement);
}

$tpl->assign('identite', $identite);
$tpl->assign('factures', $factures);
$tpl->assign('clients', $client->listAll());

$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');

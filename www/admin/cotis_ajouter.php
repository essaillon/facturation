<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess('compta', Membres::DROIT_ECRITURE);

use Garradin\DB;

$db = DB::getInstance();


$step = $radio = false;
$liste = [];

$fields = $facture->recu_fields;

if (f('select'))
{
	$form->check('add_cotis_1',[
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date',
		'membre' => 'required|numeric',
	]);

	$step = true;
}
elseif (f('add'))
{
	$form->check('add_cotis_2',[
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date',
		'membre' => 'required|numeric',
		'cotisation' => 'required',
		]);

	$radio = f('cotisation');
	
	if (!$form->hasErrors())
	{
		try
		{
			$num = (int) str_replace('cotis_', '', $radio);
			foreach($fields as $field)
			{
				$cotis[$field] = f($field.'_'.$num);
			}
			
			$cotis['date'] = date('Y-m-d', $cotis['date']);
			$cotis['expiration'] = date('Y-m-d', $cotis['expiration']);
	
			$r = $db->get('SELECT moyen_paiement, montant FROM membres_operations AS mo INNER JOIN compta_journal AS cj ON cj.id = mo.id_operation
						WHERE mo.id_cotisation = ?;', (int)$cotis['id']);
			$r = $r[0];

			$data = [
				'type_facture' => 3,
				'numero' => f('numero_facture'),
				'receveur_membre' => 1,
				'receveur_id' => f('membre'),
				'date_emission' => f('date_emission'),
				'moyen_paiement' => $r->moyen_paiement,
				'total' => $r->montant,
				'contenu' => ['id' => $cotis['id'],
							'intitule' => $cotis['intitule'],
							'souscription' => $cotis['date'],
							'expiration' => $cotis['expiration'] ]
			];

			$id = $facture->add($data);

			Utils::redirect(PLUGIN_URL . 'facture.php?id='.(int)$id);
		}
		catch (UserException $e)
		{
			$form->addError($e->getMessage());
		}
	}

	$step = true;
}


if ($step)
{
	try
	{
		$r = $facture->getCotis((int)f('membre'));
		// Passe les expiration nulles (cotis ponctuelle) à 0 pour avoir moins d'embrouilles
		foreach ($r as $i=>$cotis)
		{
			foreach($cotis as $k=>$v)
			{
				if (in_array($k, $fields))
				{
					$liste[$i][$k] = $v;
				}
			}
			if($liste[$i]['expiration'] < 0)
			{
				$liste[$i]['expiration'] = 0;
			}
		}

	}
	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}
}


$tpl->assign('liste', $liste);
$tpl->assign('radio', $radio);

$tpl->assign('step', $step);
$tpl->assign('identite', $identite);
$tpl->assign('membre_id', f('membre') ?: -1);
$tpl->assign('membres', (array)DB::getInstance()->get('SELECT id, '.$identite.' FROM membres WHERE id_categorie != -2 NOT IN (SELECT id FROM membres_categories WHERE cacher = 1);'));

$tpl->display(PLUGIN_ROOT . '/templates/cotis_ajouter.tpl');
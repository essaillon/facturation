<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess('compta', Membres::DROIT_ECRITURE);

use Garradin\DB;

qv(['id' => 'required|numeric']);
$id = (int) qg('id');

if (!$f = $facture->get($id))
{
	throw new UserException("Ce document n'existe pas.");
}

$cats = new Compta\Categories;

// Traitement

if(f('save'))
{
	$form->check('modifier_facture', [
		'type' => 'required|in:facture,devis,cerfa,cotis',
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date',
		'date_echeance' => 'required|date',
		// 'reglee' => '',
		// 'archivee' => '',
		'base_receveur' => 'required|in:membre,client',
		// 'client' => '',
		// 'membre' => '',
		'moyen_paiement' => 'required|in:' . implode(',', array_keys($cats->listMoyensPaiement())),
		'designation' => 'array|required',
		'prix' => 'array|required'
	]);

	if (!$form->hasErrors())
	{

		try
		{
			if ( count(f('designation')) !== count(f('prix')) )
			{
				throw new UserException('Nombre de désignations et de prix reçus différent.');
			}

			$truc = [
				'numero' 			=> f('numero_facture'),
				'date_emission'		=> f('date_emission'),
				'date_echeance'		=> f('date_echeance'),
				'reglee'			=> f('reglee') == 'on'?1:0,
				'archivee'			=> f('archivee') == 'on'?1:0,
				'moyen_paiement'	=> f('moyen_paiement'),
				'toto'				=> 0
			];

			if (f('type') == 'devis')
			{
				$truc['type_facture'] = 0;
			}
			elseif (f('type') == 'facture')
			{
				$truc['type_facture'] = 1;
			}
			elseif (f('type') == 'cerfa')
			{
				$truc['type_facture'] = 2;
			}
			elseif (f('type') == 'cotis')
			{
				$truc['type_facture'] = 3;
			}

			foreach(f('designation') as $k=>$value)
			{
				$truc['contenu'][$k]['designation'] = $value;
				$truc['contenu'][$k]['prix'] = f('prix')[$k];
				$truc['toto'] += f('prix')[$k];
			}
			$truc['total'] = $truc['toto'];
			unset($truc['toto']);

			if (f('base_receveur') == 'client')
			{
				$truc['receveur_membre'] = 0;
				$truc['receveur_id'] = f('client_id');
			}
			elseif (f('base_receveur') == 'membre')
			{
				$truc['receveur_membre'] = 1;
				$truc['receveur_id'] = f('membre_id');
			}

			$r = $facture->edit($id, $truc);

			Utils::redirect(PLUGIN_URL . 'facture.php?id='.(int)$id);

		}
		catch(UserException $e)
		{
			$form->addError($e->getMessage());
		}
	}
}

// Affichage

$doc['moyens_paiement']	= $cats->listMoyensPaiement();
$doc['moyen_paiement']	= $f->moyen_paiement;
$doc['type'] 			= $facture->type[$f->type_facture];
$doc['numero_facture']	= $f->numero;
$doc['reglee']			= $f->reglee?'on':'off';
$doc['base_receveur']	= $f->receveur_membre?'membre':'client';
$doc['client_id']		= $f->receveur_id;
$doc['membre_id']		= $f->receveur_id;

$tpl->assign('doc', $doc);

$tpl->assign('date_emission', strtotime(f('date_emission')) ?: $f->date_emission); // Smarty m'a saoulé pour utiliser form_field|date_fr:---
$tpl->assign('date_echeance', strtotime(f('date_echeance')) ?: $f->date_echeance); // Du coup j'utilise form_field pour ces champs


// C'est un peu l'équivalent de form_field, mais j'avais écrit ça avant
// et oulala, c'est un peu complexe, faudrait réfléchir keskivomieux
if (($d = f('designation')) && ($p = f('prix')))
{
	foreach($d as $k=>$v)
	{
		if ($v == '' && $p[$k] == 0)
		{
			continue;
		}
		$designations[] = $v;
		$prix[] = $p[$k];
	}
}
else
{
	foreach($f->contenu as $k=>$v)
	{
		if ($v['designation'] == '' && $v['prix'] == 0)
		{
			continue;
		}
		$designations[] = $v['designation'];
		$prix[] = $v['prix'];
	}
}
$tpl->assign('designations', $designations);
$tpl->assign('prix', $prix);
$tpl->assign('identite', $identite);
$tpl->assign('membres', (array)DB::getInstance()->get('SELECT id, '.$identite.' FROM membres WHERE id_categorie != -2 NOT IN (SELECT id FROM membres_categories WHERE cacher = 1);'));
$tpl->assign('clients', $client->listAll());

$tpl->display(PLUGIN_ROOT . '/templates/facture_modifier.tpl');